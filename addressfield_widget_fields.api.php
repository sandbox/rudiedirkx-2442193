<?php

/**
 * Implements hook_addressfield_widget_fields_pre_render_alter().
 *
 * @see addressfield_widget_fields_entity_form_pre_render()
 */
function hook_addressfield_widget_fields_pre_render_alter(&$element, $context) {
  // Unique id for this address field in this bundle in this entity type.
  $id = $context['entity_type'] . ':' . $context['bundle'] . ':' . $context['field_name'];

  // Move field_store_owner to the very top.
  if ($id == 'node:office:field_address') {
    $element['field_store_owner']['#weight'] = -300;
  }

  // Always hide the 'premise' component.
  $element['street_block']['premise']['#access'] = FALSE;
}

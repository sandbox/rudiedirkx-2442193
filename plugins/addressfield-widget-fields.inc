<?php

$plugin = array(
  'title' => t('Address field widget fields'),
  'format callback' => 'addressfield_widget_fields_format_widget_fields',
  'type' => 'address',
  'weight' => 99,
);

/**
 * Addressfield 'format' callback for 'addressfield-widget-fields'.
 */
function addressfield_widget_fields_format_widget_fields(&$format, $address, $context = array()) {
  if (@$context['mode'] != 'render') {
    return;
  }

  $instance = $context['instance'];
  $settings = $instance['widget']['settings'];
  $delta = $context['delta'];

  if (empty($context['entity'])) {
    drupal_set_message(t("Addressfield Widget Fields needs patch d.o/2442581#1 to display extra values correctly. If you don't need that, disable this plugin for display."), 'warning', FALSE);
    return;
  }

  if (!empty($settings['addressfield_widget_fields'])) {
    $entity_type = $instance['entity_type'];
    $entity = clone $context['entity'];
    unset($entity->{ $instance['field_name'] });
    list($id) = entity_extract_ids($entity_type, $entity);

    $rendered = entity_view($entity_type, array($id => $entity), 'addressfield_widget_fields');
    $rendered = $rendered[$entity_type][$id];
    foreach ($settings['addressfield_widget_fields'] as $field_name) {
      if (isset($rendered[$field_name])) {
        $format[$field_name] = $rendered[$field_name];
        $format[$field_name]['#weight'] += 200;

        // Keep only the relevant delta.
        $format[$field_name]['#items'] = array($delta => $format[$field_name]['#items'][$delta]);
      }
    }
  }
}
